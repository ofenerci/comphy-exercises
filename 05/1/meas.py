import sys
import numpy as np
import pandas
import matplotlib as mpl
import matplotlib.pyplot as plt

L = int(sys.argv[1].split("-")[1])
T = float(sys.argv[1].split("-")[2][:-4])

t, v_ges, E_kin, E_pot = pandas.read_table(sys.argv[1], sep=' ').values.T

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, E_kin, 'b-', label=r"$E_\text{kin}$")
ax.plot(t, E_pot, 'g-', label=r"$E_\text{pot}$")
ax.plot(t, E_pot + E_kin, 'c-', label=r"$E_\text{ges}$")
ax.plot(t, v_ges, 'r-', label=r"$v_\text{ges}$")
ax.set_xlabel("$t$")
ax.text(1, -0.06, "$L = {:d}$, $T = {:.2f}$".format(L, T), transform=ax.transAxes, horizontalalignment="right")
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=4, mode="expand", borderaxespad=0.)
fig.savefig(sys.argv[2])
