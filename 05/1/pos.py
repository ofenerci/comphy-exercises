import numpy as np
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

mpl.rc('text', usetex=False)
mpl.rc('savefig', bbox='standard')
mpl.rc('figure', autolayout=False, figsize=(6, 6))

time = 100000

x, y = np.loadtxt('pos.csv', unpack=True)
x = np.split(x, time)
y = np.split(y, time)

for t in range(0, time, 300):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title('t = {:06}'.format(t))
    ax.plot(x[t], y[t], 'r.')
    ax.set_xlim(0, 8)
    ax.set_ylim(0, 8)
    ax.set_aspect('equal')
    fig.savefig('{:06}.png'.format(t))
    print(t)
