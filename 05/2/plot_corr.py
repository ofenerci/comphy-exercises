#!/usr/bin/env python
from pylab import *
import sys

g = loadtxt(sys.argv[1]).T

x = linspace(0, 0.5, len(g))

plot(x, g)
xlabel(r'$\frac{r}{L / 2}$')
ylabel('$N$')
savefig('{}.pdf'.format(sys.argv[1][:-4]))
clf()
