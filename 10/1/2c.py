import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

T, E, M, C = np.loadtxt("2c.csv", unpack=True)

fig = plt.figure()
ax1 = fig.add_subplot(3, 1, 1)
ax1.plot(T, E, "bx")
ax1.set_xlim(0.5, 3.5)
ax1.set_xticks(T)
plt.setp(ax1.get_xmajorticklabels(), visible=False)
ax1.set_ylabel("$E$")
ax2 = fig.add_subplot(3, 1, 2)
ax2.plot(T, M, "bx")
ax2.set_xlim(0.5, 3.5)
ax2.set_xticks(T)
plt.setp(ax2.get_xmajorticklabels(), visible=False)
ax2.set_ylabel("$M$")
ax3 = fig.add_subplot(3, 1, 3)
ax3.plot(T, C, "bx")
ax3.set_xlim(0.5, 3.5)
ax3.set_xticks(T)
ax3.set_xlabel("$T$")
ax3.set_ylabel("$C$")
fig.savefig("2c.pdf")
