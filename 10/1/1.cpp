#include <cmath>
#include <cstdio>
#include <functional>
#include <random>

using namespace std;

double m(double H, size_t T)
{
	uniform_real_distribution<double> dist(0, 1);
	mt19937 gen;
	function<double()> random = bind(dist, gen);

	double s = 1;
	double E = -s * H;
	double m = s;
	for (size_t t = 0; t < T; t++)
	{
		double dE = s * H - E;
		if (dE < 0 || random() < exp(-dE))
		{
			s *= -1;
			E = -s * H;
		}
		m += s;
	}

	return m / T;
}

int main()
{
	size_t N = 200;
	FILE *file = fopen("1.csv", "w");
	for (size_t n = 0; n < N; n++)
	{
		double H = n / (N / 10.0) - 5;
		fprintf(file, "%.20f %.20f\n", H, m(H, 1000000));
	}
	fclose(file);
	return 0;
}
