#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <tuple>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::ArrayXXi;

double E(size_t N, const ArrayXXi &a, size_t x, size_t y)
{
	double E = 0;
	E -= a(x, y) * a(x, (y + 1 + N) % N);
	E -= a(x, y) * a(x, (y - 1 + N) % N);
	E -= a(x, y) * a((x + 1 + N) % N, y);
	E -= a(x, y) * a((x - 1 + N) % N, y);
	return E;
}

tuple<double, double> mc_sweep(size_t N, double T, ArrayXXi &a, function<size_t()> randomN, function<double()> randomR)
{
	double dE_ges = 0;
	double dS = 0;
	for (size_t n = 0; n < N * N; n++)
	{
		size_t x = randomN();
		size_t y = randomN();
		double dE = -2 * E(N, a, x, y);
		if (dE <= 0 || randomR() < exp(- dE / T))
		{
			a(x, y) *= -1;
			dE_ges += dE;
			dS += 2 * a(x, y);
		}
	}
	return make_tuple(dE_ges, dS);
}

tuple<double, double, double> mc(double T, function<ArrayXXi(size_t, mt19937&)> a0, const char *name0, bool mittel)
{
	size_t N = 100;
	size_t sweeps0 = 100;
	size_t sweeps = 1e4;

	mt19937 gen;
	uniform_int_distribution<size_t> distN(0, N - 1);
	function<size_t()> randomN = bind(distN, gen);

	uniform_real_distribution<double> distR(0, 1);
	function<double()> randomR = bind(distR, gen);

	ArrayXXi a = a0(N, gen);

	double E_ges = 0;
	double S_ges = 0;
	for (size_t x = 0; x < N; x++)
	{
		for (size_t y = 0; y < N; y++)
		{
			E_ges += E(N, a, x, y);
			S_ges += a(x, y);
		}
	}

	double *E = new double[sweeps0];
	double *S = new double[sweeps0];
	for (size_t t = 0; t < sweeps0; t++)
	{
		double dE, dS;
		tie(dE, dS) = mc_sweep(N, T, a, randomN, randomR);
		E_ges += dE;
		S_ges += dS;
		E[t] = E_ges;
		S[t] = S_ges;
	}

	char name[256];
	sprintf(name, "2a-%.2f-%s.csv", T, name0);
	ofstream of(name);
	of << a << endl;
	of.close();

	sprintf(name, "2b-%.2f-%s.csv", T, name0);
	FILE *file = fopen(name, "w");
	for (size_t t = 0; t < sweeps0; t++)
	{
		fprintf(file, "%zu %.20f %.20f\n", t, E[t], N * N * S[t]);
	}
	fclose(file);

	if (!mittel)
	{
		return make_tuple(0, 0, 0);
	}

	double E_mittel = E_ges;
	double E2_mittel = E_ges * E_ges;
	double S_mittel = S_ges;
	for (size_t t = 0; t < sweeps; t++)
	{
		double dE, dS;
		tie(dE, dS) = mc_sweep(N, T, a, randomN, randomR);
		E_ges += dE;
		S_ges += dS;
		E_mittel += (E_ges - E_mittel) / (t + 2);
		E2_mittel += (E_ges * E_ges - E2_mittel) / (t + 2);
		S_mittel += (S_ges - S_mittel) / (t + 2);
	}
	return make_tuple(E_mittel, N * N * S_mittel, (E2_mittel - E_mittel * E_mittel) / (T * T));
}

ArrayXXi ones(size_t N, mt19937 &gen)
{
	ArrayXXi a = ArrayXXi::Constant(N, N, 1);
	return a;
}

ArrayXXi randoms(size_t N, mt19937 &gen)
{
	uniform_int_distribution<int> distS(0, 1);
	function<int()> randomS = [&gen, &distS]() { return 2 * distS(gen) - 1; };
	return ArrayXXi::Zero(N, N).unaryExpr([&randomS](int x) { return randomS(); });
}

int main()
{
	Eigen::initParallel();

	double T[] = {1, 2.25, 3};
	double E[3];
	double M[3];
	double C[3];
	#pragma omp parallel for
	for (size_t i = 0; i < 3; i++)
	{
		mc(T[i], ones, "ones", false);
		tie(E[i], M[i], C[i]) = mc(T[i], randoms, "randoms", true);
	}

	FILE* file = fopen("2c.csv", "w");
	for (size_t i = 0; i < 3; i++)
	{
		fprintf(file, "%.20f %.20f %.20f %.20f\n", T[i], E[i], M[i], C[i]);
	}
	fclose(file);

	return 0;
}
