import math as m
import numpy as np
import matplotlib.pyplot as plt

H, m = np.loadtxt("1.txt", unpack=True)

H_ana = np.linspace(H[0], H[-1], 10000)
plt.plot(H, m, "bx")
plt.plot(H_ana, np.tanh(H_ana), "r-")
plt.xlabel("$H$")
plt.ylabel(r"$\langle s \rangle$")
plt.savefig("1.pdf")
