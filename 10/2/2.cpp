#include <Eigen/Dense>
#include <random>
#include <iostream>

using Eigen::ArrayXXi;
using std::mt19937_64;
using std::uniform_int_distribution;

ArrayXXi setInitialLattice(int s, ArrayXXi:Index dim)
{
	ArrayXXi lattice = ArrayXXi::Constant(dim, dim, s);
	return lattice;
}

double energy_shift(int x, int y, int initial)
{


void iterate(ArrayXXi lattice, size_t steps, size_t dim)
{
	ArrayXXi lattice = setInitialLattice(1, dim);
	mt19937_64 rand(42);
	uniform_int_distribution<int> uniform(1, dim-1);
	for(size_t t = 0; t < steps; t++)
	{
		x = uniform(rand);
		y = uniform(rand);
		dE = energy_shift(x, y, lattice[x][y]);
		
	}

int main()
{
	return 0;
}
