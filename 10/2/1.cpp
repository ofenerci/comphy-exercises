#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <vector>
#include <random>

using std::cout;
using std::endl;
using std::mt19937_64;
using std::uniform_real_distribution;
using std::uniform_int_distribution;
using Eigen::VectorXd;
using Eigen::MatrixXd;

double worka(double H, unsigned int N, int seed)
{
	int s = 1;
	double delE;
	mt19937_64 rand(seed);
	uniform_real_distribution<double> uniform(0, 1);
	double O = 0;

	for(unsigned int i = 1; i < N; i++)
	{
		if (s == 1)
		{
			delE = 2 * H;
			if (delE <= 0)
			{
				s = -1;
			}
			else
			{
				if (uniform(rand) < exp(-delE))
				{
					s = -1;
				}
			}
		}
		else if (s == -1)
		{
			delE = -2 * H;
			if (delE <= 0)
			{
				s = 1;
			}
			else
			{
				if (uniform(rand) < exp(-delE))
				{
					s = 1;
				}
			}
		}
		O += s;
	}
	O /= N;

	return O;
}

void plota(size_t N)
{
	double H_min = -5;
	double h = 1e-1;

	mt19937_64 rand(42);
	uniform_int_distribution<int> uniform(1, 10000);

	FILE* outfile = fopen("1.txt", "w");
	for (size_t i = 0; i < N; i++)
	{
		double H = H_min + i * h;
		fprintf(outfile, "%.15f %.15f\n", H, worka(H, 1000000, uniform(rand)));
	}
}

int main()
{
	plota(100);
	return 1;
}
