import itertools
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

f = lambda x, a, b, c, d: a * np.tanh(b * x + c) + d
x = np.linspace(0, 1, 1000)

params = []

for L in [10, 50, 100]:
        p, q, _ = np.loadtxt("c-{}.csv".format(L), unpack=True)
        popt, pcov = scipy.optimize.curve_fit(f, p, q, p0=[0.5, 1, 0.6, 0.5])
        params.append(popt)
        line = ax.plot(p, q, "x")
        ax.plot(x, f(x, *popt), "-", c=line[0].get_color(), label="$L = {}$".format(L))

pc = []
for i, j in itertools.combinations(range(3), 2):
        pc.append(scipy.optimize.newton(lambda x, p1, p2: f(x, *p1) - f(x, *p2), 0.6, args=(params[i], params[j])))

pc = np.mean(pc)
open('pc.csv', 'w').write(str(pc))
fig.text(0.3, 0.4, "$p_c = {}$".format(pc))
ax.set_xlabel("$p$")
ax.set_ylabel("$q_L(p)$")
ax.legend(loc='upper left')
fig.savefig("c.pdf")
