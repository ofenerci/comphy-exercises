
#include<functional>

#include <queue>
#include <tuple>
#include <random>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


using namespace std;

using Eigen::ArrayXXi;

void write_to_file(ArrayXXi arr, string fname) {

    ofstream file;
    file.open(fname);
    file << arr << endl;
    file.close();

    return;

}

int color(ArrayXXi &lattice, int x, int y, int c) {

    queue<pair<int,int>> cand;
    int N = lattice.outerSize();
    int clusterSize = 0;
    int i, j;

    cand.push(pair<int,int>(x, y));

    while (!cand.empty()) {
        tie(i, j) = cand.front();
        cand.pop();
        clusterSize++;

        if (i - 1 >= 0) {
            if (lattice(i-1, j) == 1) {
                lattice(i - 1, j) = c;
                cand.push(pair<int,int>(i - 1, j));
            }
        }
        if (i + 1 < N) {
            if (lattice(i+1, j) == 1) {
                lattice(i + 1, j) = c;
                cand.push(pair<int,int>(i + 1, j));
            }
        }
        if (j - 1 >= 0) {
            if (lattice(i, j-1) == 1) {
                lattice(i, j - 1) = c;
                cand.push(pair<int,int>(i, j - 1));
            }
        }
        if (j + 1 < N) {
            if (lattice(i, j+1) == 1) {
                lattice(i, j + 1) = c;
                cand.push(pair<int,int>(i, j + 1));
            }
        }
    }
    return clusterSize;
}

pair<bool,int> percolateArray(int N, double p, std::function<double()> gen, bool save=false) {

    ArrayXXi lattice = ArrayXXi::Zero(N, N);
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            lattice(i, j) = gen() <= p ? 1 : 0;
        }
    }

    bool percolates = false;
    int counter = 2;
    int maxClusterSize = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (lattice(j, i) == 1) {
                int size = color(lattice, j, i, counter);

                bool upper = false;
                bool lower = false;
                bool right = false;

                for (int k = 0; k < N; k++) {

                    if (lattice(k, N - 1) == counter) {
                        upper = true;
                    }
                    if (lattice(k, 0) == counter) {
                        lower = true;
                    }
                    if (lattice(N - 1, k) == counter) {
                        right = true;
                    }

                    if (upper && lower && right) {
                        percolates = true;
                        size = 0;
                    }
                }
                maxClusterSize = size > maxClusterSize ? size : maxClusterSize;
                counter++;
            }
        }
    }
    if (save) {
        write_to_file(lattice, "array_" + to_string(N) + "_" + to_string(p) + "_.txt");
    }
    return pair<bool,int>(percolates, maxClusterSize);
}


int main() {
    std::random_device rd;
    std::mt19937_64 rand(rd());
    std::uniform_real_distribution<double> uniform(0, 1);
    auto gen = [&rand, &uniform]() -> double { return uniform(rand); };

    // b)
    // siehe array_*.pdf
    percolateArray(50, 0.1, gen, true);
    percolateArray(50, 0.5, gen, true);
    percolateArray(50, 0.9, gen, true);

    // c)
    // Siehe plot.py hist.pdf
    
    int len = 100;
    double dp = 1.0/len;
    int R = 500;
    bool percolates;
    int size;

    for (int L: {10, 25, 50, 75, 100, 200}) {
        vector<int> hist(len, 0);
        for (int i=0; i<len; i++) {
            for (int j=0; j<R; j++) {
                tie(percolates, size) = percolateArray(L, dp*i, gen);
                if (percolates) {
                    hist[i]++;
                }
            }
        }
        ofstream file;
        file.open("hist_" + to_string(L) + ".txt");
        for (auto x: hist) {
            file << x << " ";
        }
        file.close();
    }
    
    // d)
    // Siehe plot.py und hist.pdf

    // e)
    // siehe hist_beta.pdf
    vector<double> hist(len, 0);
    int L = 200;
    int runs = 10;
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < runs; j++) {
            tie(percolates, size) = percolateArray(L, dp * i, gen);
            hist[i] += static_cast<double>(size) / (L*L) / runs;
        }
    }
    ofstream file;
    file.open("hist_beta.txt");
    for (auto x: hist) {
        file << x << " ";
    }
    file.close();
    
    return 0;
