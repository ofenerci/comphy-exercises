#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;

void ex1(float x, float y)
{
	float a_n;
	float b_n;
	float a_c;
	float b_c;
	float rel_error_a;
	float rel_error_b;
	
	// naive calculation

	a_n = 1 / sqrt(x) - 1 / sqrt(x + 1);
	b_n = (1 - cos(y)) / sin(y);

	// clever calculation
	// a) either factor out sqrt(x) or sqrt(x + 1)
	//    error reduces as exactly 1 is subtracted of something near by 1
	// b) by using an alternate form, subtraction vanishes
	
	a_c = sqrt(x) * (sqrt(1 + 1 / x) - 1) / sqrt(pow(x, 2) + x);
	b_c = 2 * pow(sin(y / 2), 2) / sin(y);
	rel_error_a = abs(a_n - a_c) / abs(a_c) * 100;
	rel_error_b = abs(b_n - b_c) / abs(b_c) * 100;
	
	cout << "Results of naive calculations" << endl;
	cout << "a : " << a_n << endl;
	cout << "b : " << b_n << endl;
	cout << "Results of clever calculations" << endl;
	cout << "a : " << a_c << endl;
	cout << "b : " << b_c << endl;
	cout << "Relative error" << endl;
	cout << "(a) " << rel_error_a << " %" << endl;
	cout << "(b) " << rel_error_b << " %" << endl;
}

int main(int argc, char *argv[])
{
	ex1(10000, 0.001);
//	ex1(strtof(argv[1], NULL), strtof(argv[1], NULL));
}
