#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;

void ex2a(double y_0, double h, double x)
{
	double y_2 = 1;
	double y_1 = 1 - h;
	double num_sol;
	double ana_sol;
	double rel_err;
	double n = (int) x / h;

	for (int i = 2; i <= (int)(x / h); i++)
	{
		num_sol = -2 * h * y_1 + y_2;
		y_2 = y_1;
		y_1 = num_sol;
	}

	// analytic solution
	ana_sol = exp(-x);


	// relative error
	rel_err = abs(num_sol - ana_sol) / abs(ana_sol) * 100;

	cout << "(a)" << endl;
	cout << "numerical solution: " << num_sol << endl;
	cout << "analytic solution: " << ana_sol << endl;
	cout << "relative error: " << rel_err << "%" << endl << endl;
	
}

void ex2b(double y_0, double h, double x)
{
	double y_2 = 1;
	double y_1 = 1 - h;
	double num_sol;
	double ana_sol;
	double rel_err;
	double n = (int) x / h;

	for (int i = 2; i <= (int)(x / h); i++)
	{
		num_sol = 2 * h * y_1 + y_2;
		y_2 = y_1;
		y_1 = num_sol;
	}

	// analytic solution
	ana_sol = exp(x);


	// relative error
	rel_err = abs(num_sol - ana_sol) / abs(ana_sol) * 100;

	cout << "(b)" << endl;
	cout << "numerical solution: " << num_sol << endl;
	cout << "analytic solution: " << ana_sol << endl;
	cout << "relative error: " << rel_err << "%" << endl << endl;
	
}

int main(int argc, char *argv[])
{
	ex2a(1, 0.001, 5);
	ex2b(1, 0.001, 5);
}
