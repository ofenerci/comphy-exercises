#include <functional>
#include <vector>

using namespace std;

void work(function<double(double, double)> f, double x0, double r_max, size_t n_r, size_t n_x, size_t n_start, const char *name)
{
	vector<vector<double>> v(n_r, vector<double>(n_x, 0));
	for (size_t i = 0; i < n_r; i++)
	{
		double r = i * r_max / n_r;
		double x = x0;
		for (size_t j = 0; j < n_start; j++)
		{
			x = f(x, r);
		}
		for (size_t j = 0; j < n_x; j++)
		{
			x = f(x, r);
			v[i][j] = x;
		}
	}
	FILE *file = fopen(name, "w");
	for (size_t i = 0; i < n_r; i++)
	{
		fprintf(file, "%.15f", i * r_max / n_r);
		for (size_t j = 0; j < n_x; j++)
		{
			fprintf(file, " %.15f", v[i][j]);
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

int main()
{
	work([](double x, double r) {return r * x * (1 - x);}, 0.5, 4, 1000, 100, 1000, "1-logistic.csv");
	work([](double x, double r) {return r * x - x * x * x;}, 0.5, 3, 1000, 100, 1000, "1-cubic+.csv");
	work([](double x, double r) {return r * x - x * x * x;}, -0.5, 3, 1000, 100, 1000, "1-cubic-.csv");
}
