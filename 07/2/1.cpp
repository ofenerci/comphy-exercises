#include <functional>
#include <iostream>
#include <string>

using namespace std;

void iterateMap(double x_0, double r_0, double r_max, double h, unsigned int fIter, function<double(double, double)> map, string filename)
{
	//iterate fIter times before plotting

	double N = (r_max - r_0) / h;

	FILE *file = fopen(filename.c_str(), "w");

	for(unsigned int j = 0; j < N; j++)
	{
		double x = x_0;
		for(unsigned int i = 0; i < fIter; i++)
		{
			x = map(r_0 + j * h, x);
		}

		fprintf(file, "%.15f", r_0 + j * h);
		for(unsigned int i = 0; i < 100; i++)
		{
			x = map(r_0 + j * h, x);
			fprintf(file, " %.15f", x);
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

int main()
{
	double x_0 = 0.5;
	double r_0 = 0;
	double r_max = 4;
	double h = 0.005;
	unsigned int fIter = 100;

	auto logisticMap = [](double r, double x){return r * x * (1 - x);};

	iterateMap(x_0, r_0, r_max, h, fIter, logisticMap, "a_logistic.csv");

	auto cubicMap = [](double r, double x){return r * x - x * x * x;};

	r_0 = 0;
	r_max = 3;

	iterateMap(x_0, r_0, r_max, h, fIter, cubicMap, "ap_cubic.csv");
	iterateMap(-x_0, r_0, r_max, h, fIter, cubicMap, "am_cubic.csv");
}
