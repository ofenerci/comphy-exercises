#include <Eigen/Dense>
#include <iostream>

using Eigen::Matrix4d;
using Eigen::SelfAdjointEigenSolver;
using Eigen::VectorXd;
using std::cout;
using std::endl;
using Eigen::MatrixXd;

void eigenvaluesEigen()
{
	Matrix4d A;
	A << 1, 2, 2, 4,
	     2, 5, 2, 1,
	     2, 2, 6, 3,
	     4, 1, 3, 4;
	SelfAdjointEigenSolver<Matrix4d> dA(A);

	cout << "Eigenwerte mit Eigen" << endl;
	cout << dA.eigenvalues().transpose() << endl << endl;
}

void eigenvaluesPotenz(MatrixXd A, double epsilon)
{
	VectorXd tmp;

	VectorXd sv = VectorXd::Constant(A.cols(), 1);
	cout << "Eigenwerte mit Potenzmethode" << endl;

	for (MatrixXd::Index i = 0; i < A.cols(); i++)
	{
		VectorXd ev = sv;
		for(int i = 0; i < 1000; i++)
		{
			tmp = ev;
			ev = (A * ev).normalized();
		}

		double big_ew = ev.dot(A * ev);
		A = A - big_ew * ev * ev.transpose();
		cout << big_ew << ",  ";
	}
	cout << endl;
}

int main()
{
	MatrixXd A = MatrixXd(4, 4);
	A << 1, 2, 2, 4,
	     2, 5, 2, 1,
	     2, 2, 6, 3,
	     4, 1, 3, 4;
	VectorXd sv = VectorXd(4);
	eigenvaluesEigen();
	eigenvaluesPotenz(A, 1);
	return 1;
}
