#include <cmath>
#include <Eigen/Dense>
#include <iostream>

using std::cout;
using std::endl;
using Eigen::MatrixXd;
using Eigen::SelfAdjointEigenSolver;

MatrixXd buildHamiltonian(MatrixXd::Index N)
{
	MatrixXd H = MatrixXd::Zero(N, N);

	for (MatrixXd::Index i = 0; i < N; i++)
	{
		//build diagonal matrix
		H(i, i) = 2 * i + 1 + 0.2 * 0.25 * (6 * i * i + 6 * i + 3);

		//build off-diagonal matrix
		for (MatrixXd::Index j = 0; j < N; j++)
		{
			if (i == j - 4)
			{
				H(i, j) += 0.2 * 0.25 * sqrt(j * (j - 1) * (j - 2) * (j - 3));
			}
			else if(i == j + 4)
			{
				H(i, j) += 0.2 * 0.25 * sqrt((j + 1) * (j + 2) * (j + 3) * (j + 4));
			}
			else if(i == j - 2)
			{
				H(i, j) += 0.2 * 0.25 * sqrt(j + (j - 1)) * (4 * j - 2);
			}
			else if(i == j + 2)
			{
				H(i, j) += 0.2 * 0.25 * sqrt((j + 1) * (j + 2)) * (4  * j + 6);
			}
		}
	}
	return H;
}

void workC (MatrixXd::Index N)
{
	cout << "Eigenwerte mit Eigen:" << endl;
	MatrixXd H = buildHamiltonian(N);
	SelfAdjointEigenSolver<MatrixXd> dH (H);
	for(int i = 0; i < 10; i++)
	{
		cout << dH.eigenvalues()(i) << ", ";
	}
	cout << endl;
}

int main()
{
	workC(50);
}

