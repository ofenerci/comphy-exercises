#include <cmath>
#include <iostream>
#include <tuple>

#include <Eigen/Dense>

using namespace std;

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::SelfAdjointEigenSolver;

VectorXd eigenvalues(const MatrixXd &M)
{
	SelfAdjointEigenSolver<MatrixXd> solver(M);
	return solver.eigenvalues();
}

void b(double lambda)
{
	double dx = 0.1;
	size_t L = 10;
	size_t N = 10 * 2 * L + 1;

	MatrixXd tmpU = MatrixXd::Zero(N, N);
	tmpU.topRightCorner(N - 1, N - 1) = MatrixXd::Identity(N - 1, N - 1);

	MatrixXd tmpL = MatrixXd::Zero(N, N);
	tmpL.bottomLeftCorner(N - 1, N - 1) = MatrixXd::Identity(N - 1, N - 1);

	MatrixXd tmpR = VectorXd::LinSpaced(Eigen::Sequential, N, - (double) L, (double) L)
	                .unaryExpr([lambda, dx](double xi) {return pow(xi, 2) + lambda * pow(xi, 4);})
	                .asDiagonal();

	MatrixXd H = -1. / (dx * dx) * (tmpU + tmpL - 2 * MatrixXd::Identity(N, N)) + tmpR;

	cout << eigenvalues(H) << endl << endl;
}

void c(double lambda)
{
	size_t N = 50;

	MatrixXd tmpR4 = MatrixXd::Zero(N + 1, N + 1);
	tmpR4.topRightCorner(N - 3, N - 3) = VectorXd::LinSpaced(Eigen::Sequential, N - 3, 4, N)
	                                     .unaryExpr([](double m) { return sqrt(m * (m - 1) * (m - 2) * (m - 3)); })
	                                     .asDiagonal();

	MatrixXd tmpL4 = MatrixXd::Zero(N + 1, N + 1);
	tmpL4.bottomLeftCorner(N - 3, N - 3) = VectorXd::LinSpaced(Eigen::Sequential, N - 3, 0, N - 4)
	                                       .unaryExpr([](double m) { return sqrt((m + 1) * (m + 2) * (m + 3) * (m + 4)); })
	                                       .asDiagonal();

	MatrixXd tmpR2 = MatrixXd::Zero(N + 1, N + 1);
	tmpR2.topRightCorner(N - 1, N - 1) = VectorXd::LinSpaced(Eigen::Sequential, N - 1, 2, N)
	                                     .unaryExpr([](double m) { return sqrt(m * (m - 1)) * (4 * m - 2); })
	                                     .asDiagonal();

	MatrixXd tmpL2 = MatrixXd::Zero(N + 1, N + 1);
	tmpL2.bottomLeftCorner(N - 1, N - 1) = VectorXd::LinSpaced(Eigen::Sequential, N - 1, 0, N - 2)
	                                      .unaryExpr([](double m) { return sqrt((m + 1) * (m + 2)) * (4 * m + 6); })
	                                      .asDiagonal();

	MatrixXd tmpD = VectorXd::LinSpaced(Eigen::Sequential, N + 1, 0, N)
	                                      .unaryExpr([](double m) { return 6 * m * m + 6 * m + 3; })
	                                      .asDiagonal();

	
	MatrixXd tmpD2 = VectorXd::LinSpaced(Eigen::Sequential, N + 1, 0, N)
	                                      .unaryExpr([](double m) { return 2 * m + 1; })
	                                      .asDiagonal();

	MatrixXd H = 0.25 * lambda * (tmpL4 + tmpL2 + tmpR4 + tmpR2 + tmpD) + tmpD2;

	cout << eigenvalues(H) << endl;
}

int main()
{
	b(0);
	b(0.2);
	c(0.2);
	return 0;
}
