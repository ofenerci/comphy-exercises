#include <iostream>
#include <tuple>

#include <Eigen/Dense>

using namespace std;

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::SelfAdjointEigenSolver;

MatrixXd A(4, 4);

void a()
{
	SelfAdjointEigenSolver<MatrixXd> solver(A);
	cout << solver.eigenvalues() << endl;
}

tuple<double, VectorXd> power1(const MatrixXd &A)
{
	VectorXd v = VectorXd::Constant(4, 1);
	for (size_t i = 0; i < 1000; i++)
	{
		v = (A * v).normalized();
	}
	return make_tuple(v.dot(A * v), v);
}

VectorXd power(const MatrixXd &A)
{
	VectorXd lambda(A.rows());
	MatrixXd M(A);
	for (VectorXd::Index i = 0; i < lambda.size(); i++)
	{
		VectorXd v;
		tie(lambda[i], v) = power1(M);
		M = M - lambda[i] * v * v.transpose();
	}
	return lambda;
}

void b()
{
	cout << power(A) << endl;	
}

int main()
{
	A << 1, 2, 2, 4,
	     2, 5, 2, 1,
	     2, 2, 6, 3,
	     4, 1, 3, 4;
	a();
	cout << endl;
	b();
	return 0;
}
