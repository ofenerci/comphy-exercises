#include <cstdio>
#include "1.h"
#include <string>

using namespace std;

void writeToFile(string filename, double** y, int N, double x_0, double h)
{
    FILE *outfile = fopen(filename.c_str(), "w");
    for (int i = 0; i <= N; i++)
    {
        fprintf(outfile, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n", x_0 + h * i, y[i][0], y[i][1], y[i][2], y[i][3], y[i][4], y[i][5], y[i][6]);
    }
    fclose(outfile);
}

void noSpeed(double** y)
{
	y[0][0] = 1;
    y[0][1] = 1;
    y[0][2] = 0;
    y[0][3] = 0;
}

void parallelSpeed(double** y)
{
	y[0][0] = 1;
    y[0][1] = 1;
    y[0][2] = 1;
    y[0][3] = 1;
}

void skewSpeed(double** y)
{
	y[0][0] = 1;
    y[0][1] = 1;
    y[0][2] = 3;
    y[0][3] = 7;
}

void energyConservation(double** y, int N)
{
	for(int i = 0; i < N; i++)
	{
		y[i][4] = 0.5 * (y[i][0] * y[i][0] + y[i][1] * y[i][1]);
		y[i][5] = 0.5 * (y[i][2] * y[i][2] + y[i][3] * y[i][3]);
		y[i][6] = y[i][4] + y[i][5];
	}
}

void aufgabe2()
{
    auto F = [](double *rv, double *F)
    {
        F[0] = -rv[0];
        F[1] = -rv[1];
    };

    int N = 100000;
    double **y = new double*[N+1];
    double x_0 = 0;
    double x_n = 30;
    double h = (x_n - x_0) / N;

    for(int i = 0; i <= N; i++)
    {
        y[i] = new double[7];
    }

    noSpeed(y);
    fwork(F, 2, N, x_0, x_n, y);
    energyConservation(y, N);
    writeToFile("2v0.csv", y, N, x_0, h);

    parallelSpeed(y);
    fwork(F, 2, N, x_0, x_n, y);
    energyConservation(y, N);
    writeToFile("2vp.csv", y, N, x_0, h);

    skewSpeed(y);
    fwork(F, 2, N, x_0, x_n, y);
    energyConservation(y, N);
    writeToFile("2vs.csv", y, N, x_0, h);


    for (int i = 0; i <= N; i++)
    {
        delete[] y[i];
    }

    delete[] y;
}

int main()
{
    aufgabe2();
    return 0;
}
