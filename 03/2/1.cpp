#include <cstdio>
#include <iostream>
#include <cmath>
#include <functional>

using namespace std;

void runge_kutta(function<void(double x, double* y, double* yd)> f, int D, int N, double x_0, double x_n, double** y)
{
	double h = (x_n - x_0) / N;

	double* k1 = new double[D];
	double* k2 = new double[D];
	double* k3 = new double[D];
	double* k4 = new double[D];
	double* tmpest = new double[D];

	for(int j = 0; j < N; j++)
	{
		f(x_0 + j * h, y[j], k1);

		for(int i = 0; i < D; i++)
		{
			tmpest[i] = y[j][i] + 0.5 * h * k1[i];
		}
		f(x_0 + h / 2 + j * h, tmpest, k2);

		for(int i = 0; i < D; i++)
		{
			tmpest[i] = y[j][i] + 0.5 * h * k2[i];
		}
		f(x_0 + h / 2 + j * h, tmpest, k3);

		for(int i = 0; i < D; i++)
		{
			tmpest[i] = y[j][i] + h * k3[i];
		}
		f(x_0 + h + j * h, tmpest, k4);
		
		for(int i = 0; i < D; i++)
		{
			y[j + 1][i] = y[j][i] + h / 6 * (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]);
		}
	}
	delete [] tmpest;
	delete [] k1;
	delete [] k2;
	delete [] k3;
	delete [] k4;
}

void fwork(function<void(double* rv, double* rvd)> F, int D, int N, double x_0, double x_1, double** ys)
{
	auto f = [F, D](double t, double* y, double* yd)
	{
		for (int i = 0; i < D; i++)
		{
			yd[i] = y[i + D];
		}
		F(y, yd + D);
	};
	runge_kutta(f, 2 * D, N, x_0, x_1, ys);
}
