#include <cmath>
#include <cstdio>
#include <functional>
#include <string>
#include <vector>

#include <omp.h>

using namespace std;

void m(double theta, double &mx, double &my)
{
	mx = sin(theta);
	my = cos(theta);
}

void ferro(int x, int y, double &nx, double &ny)
{
	nx = 0;
	ny = 1;
}

void antiferro(int x, int y, double &nx, double &ny)
{
	nx = 0;
	ny = (((x + y) % 2) == 0) ? 1 : -1;
}

double E(function<void(int, int, double&, double&)> n, int N, double theta)
{
	double mx, my;
	m(theta, mx, my);

	double E = 0;
	#pragma omp parallel for reduction(+:E)
	for (int x = -N; x <= N; x++)
	{
		for (int y = -N; y <= N; y++)
		{
			if (x == 0 && y == 0)
			{
				continue;
			}

			double nx, ny;
			n(x, y, nx, ny);

			double R = sqrt(x * x + y * y);
			double Rx = x;
			double Ry = y;

			double Rm = Rx * mx + Ry * my;
			double Rn = Rx * nx + Ry * ny;
			double mn = mx * nx + my * ny;
			E += 1 / (R * R * R) * (-3 / (R * R) * Rm * Rn + mn);
		}
	}

	return E;
}

void a_work(const char *name, function<void(int, int, double&, double&)> n)
{
	vector<int> Ns {2, 5, 10};
	for (int N : Ns)
	{
		string fname("1a-");
		fname += name;
		fname += "-";
		fname += to_string(N);
		fname += ".csv";
		FILE *file = fopen(fname.c_str(), "w");
		for (double theta = 0; theta <= 2 * 3.141592654; theta += 0.001)
		{
			fprintf(file, "%.15f %.15f\n", theta, E(n, N, theta));
		}
		fclose(file);
	}
}

void a()
{
	a_work("ferro", ferro);
	a_work("antiferro", antiferro);
}

double derive(function<double(double)> f, double x, double h)
{
	return (f(x + h) - f(x - h)) / (2 * h);
}

double T(function<void(int, int, double&, double&)> n, int N, double theta)
{
	using namespace std::placeholders;
	return - derive(bind(E, n, N, _1), theta, 1e-7);
}

double T_analytic(function<void(int, int, double&, double&)> n, int N, double theta)
{
	double mx, my;
	m(theta, mx, my);
	
	double T = 0;
	#pragma omp parallel for reduction(+:T)
	for (int x = -N; x <= N; x++)
	{
		for (int y = -N; y <= N; y++)
		{
			if (x == 0 && y == 0)
			{
				continue;
			}

			double nx, ny;
			n(x, y, nx, ny);

			double R = sqrt(x * x + y * y);
			double Rx = x;
			double Ry = y;

			double Rn = Rx * nx + Ry * ny;
			T += -1 / (R * R * R * R * R) * (mx * (3 * Rn * Ry - R * R * ny) - my * (3 * Rn * Rx - R * R * nx));
		}
	}

	return T;
}

void b_work(const char *name, function<void(int, int, double&, double&)> n)
{
	vector<int> Ns {2, 5, 10};
	for (int N : Ns)
	{
		string fname("1b-");
		fname += name;
		fname += "-";
		fname += to_string(N);
		fname += ".csv";
		FILE *file = fopen(fname.c_str(), "w");
		for (double theta = 0; theta <= 2 * 3.141592654; theta += 0.001)
		{
			fprintf(file, "%.15f %.15f %.15f\n", theta, T(n, N, theta), T_analytic(n, N, theta));
		}
		fclose(file);
	}
}

void b()
{
	b_work("ferro", ferro);
	b_work("antiferro", antiferro);
}

int main()
{
	a();
	b();
	return 0;
}
