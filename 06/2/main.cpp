#include <tuple>
#include <Eigen/Dense>
#include <cmath>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;
using Eigen::MatrixXd;
using Eigen::MatrixXi;

void iterateGaussSeidel(MatrixXd &potential, const MatrixXd &rho, const MatrixXi &mask, double dx, double dy)
{
	double epsilon = 1e-5;
	bool done = false;

	while(!done)
	{
		done = true;

		for(MatrixXd::Index i = 1; i < potential.rows() - 1; i++)
		{
			for(MatrixXd::Index j = 1; j < potential.cols() - 1;  j++)
			{
				if(mask(i, j) == 0)
				{
					double tmp = 0.25 * (potential(i + 1, j) + potential(i - 1, j) + potential(i, j + 1) + potential(i, j - 1)) - 0.25 * dx * dy * rho(i, j);
					if(fabs(tmp - potential(i, j)) > epsilon)
					{
						done = false;
					}
					potential(i, j) = tmp;
				}
			}
		}
	}
}


tuple<MatrixXd, MatrixXd> getE(const MatrixXd &potential, double dx, double dy)
{
	MatrixXd Ex = MatrixXd::Zero(potential.rows(), potential.cols());
	MatrixXd Ey = MatrixXd::Zero(potential.rows(), potential.cols());

	for (MatrixXd::Index y = 1; y < potential.rows() - 1; y++)
	{
		for (MatrixXd::Index x = 1; x < potential.cols() - 1; x++)
		{
			Ex(y, x) = (potential(y, x + 1) - potential(y, x - 1)) / (2 * dx);
			Ey(y, x) = (potential(y + 1, x) - potential(y - 1, x)) / (2 * dy);
		}
	}

	return make_tuple(Ex, Ey);
}

void work(function<tuple<MatrixXd, MatrixXd, MatrixXi>(char)> init, char ex)
{
	double dx = 0.05;
	double dy = 0.05;

	MatrixXd potential;
	MatrixXd rho;
	MatrixXi mask;

	tie(potential, rho, mask) = init(ex);
	iterateGaussSeidel(potential, rho, mask, dx, dy);

	MatrixXd Ex;
	MatrixXd Ey;

	tie(Ex, Ey) = getE(potential, dx, dy);

	if(ex == 'd' || ex == 'e')
	{
		ofstream file("angle_" + string(1, ex) + ".csv");
		double E_n = 0;
		for(MatrixXd::Index i = 1; i < Ex.cols() - 1; i++)
		{
			double y = Ey(Ey.rows() - 2, i);
			double x = Ex(Ex.rows() - 2, i);

			file << atan2(y, x) << endl;

			double E_n_l = Ex(1, i) * dy;
			double E_n_r = Ex(Ex.cols() - 2, i) * dy;
			double E_n_u = Ey(1, i) * dx;
			double E_n_d = y * dx;

			E_n += E_n_l + E_n_r + E_n_u + E_n_d;
		}
		file.close();
		cout << "Numerisches Ergebnis des Linienintegrals von " + string(1, ex) << endl << E_n << endl;
	}
	if(ex != 'd')
	{
		ofstream file("potential_" + string(1, ex) + ".csv");
		file << potential << endl;
		file.close();

		file.open("Ex_" + string(1, ex) + ".csv");
		file << Ex << endl;
		file.close();

		file.open("Ey_" + string(1, ex) + ".csv");
		file << Ey << endl;
		file.close();
	}
}

tuple<MatrixXd, MatrixXd, MatrixXi> init(char ex)
{
	MatrixXd::Index Nx = 20;
	MatrixXd::Index Ny = 20;

	MatrixXd potential = MatrixXd::Zero(Nx, Ny);
	MatrixXd rho = MatrixXd::Zero(Nx, Ny);
	MatrixXi mask = MatrixXi::Zero(Nx, Ny);

	for(MatrixXd::Index i = 0; i < Ny; i++)
	{
		if(ex == 'b')
		{
			potential(i, 0) = 1;
		}
		else
		{
			potential(i, 0) = 0;
		}

		potential(i, Nx - 1) = 0;
		mask(i, 0) = true;
		mask(i, Nx - 1) = true;
	}
	for(MatrixXd::Index i = 0; i < Nx; i++)
	{
		potential(0, i) = 0;
		potential(Ny - 1, i) = 0;
		mask(0, i) = true;
		mask(Ny - 1, i) = true;
	}

	if(ex == 'c' || ex == 'd')
	{
		rho(Nx / 2, Ny / 2) = 1;
	}

	if(ex == 'e')
	{
		rho(Nx / 2, Ny / 2) = 1;
		rho(Nx / 4, Ny / 4) = 1;
		rho(3 * Nx / 4, 3 * Ny / 4) = 1;
	}

	return make_tuple(potential, rho, mask);
}


int main()
{
	work(init, 'b');
	work(init, 'c');
	work(init, 'd');
	work(init, 'e');
}
