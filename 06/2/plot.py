import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


for ex in ['b', 'c', 'e']:
    potential = np.loadtxt('potential_{}.csv'.format(ex))
    Ex = np.loadtxt('Ex_{}.csv'.format(ex))
    Ey = np.loadtxt('Ey_{}.csv'.format(ex))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(potential, origin='upper')
    fig.colorbar(im, ax=ax)
    fig.savefig('potential_{}.pdf'.format(ex))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
#    im = ax.imshow(np.sqrt(Ex**2 + Ey**2))
#    fig.colorbar(im, ax=ax)
    ax.quiver(Ex, Ey, pivot='middle')
    ax.invert_yaxis()
   # ax.quiver(Ex / np.sqrt(Ex**2 + Ey**2), Ey / np.sqrt(Ex**2 + Ey**2), np.sqrt(Ex**2 + Ey**2), pivot='middle')
    fig.savefig('E_{}.pdf'.format(ex))

for ex in ['d', 'e']:
    angle = np.loadtxt('angle_{}.csv'.format(ex), unpack=True)
    x = np.linspace(0, 1, len(angle))
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(x, angle)
    fig.savefig('angle_{}.pdf'.format(ex))
