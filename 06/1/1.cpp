#include <cmath>
#include <complex>
#include <fstream>
#include <iostream>
#include <vector>

#include <Eigen/Dense>

using std::complex;
using std::cout;
using std::endl;
using std::ofstream;
using std::vector;

using Eigen::MatrixXcd;
using Eigen::VectorXcd;
using Eigen::VectorXd;

typedef complex<double> C;

vector<VectorXcd> crank_nicholson(const MatrixXcd &H, const VectorXcd &psi0, double dt, size_t T)
{
	vector<VectorXcd> psi(T, VectorXcd());
	psi[0] = psi0;

	MatrixXcd S = (MatrixXcd::Identity(H.rows(), H.cols()) + C(0, 1) / 2. * dt * H).inverse() * (MatrixXcd::Identity(H.rows(), H.cols()) - C(0, 1) / 2. * dt * H);

	for (size_t i = 1; i < T; i++)
	{
		psi[i] = S * psi[i - 1];
	}

	return psi;
}

VectorXd calc_norm(const vector<VectorXcd> &psi)
{
	VectorXd norm(psi.size());
	for (VectorXd::Index i = 0; i < norm.size(); i++)
	{
		norm[i] = psi[i].norm();
	}
	return norm;
}

VectorXd calc_mean(const vector<VectorXcd> &psi, const VectorXd &xi)
{
	VectorXd mean(psi.size());
	for (VectorXd::Index i = 0; i < mean.size(); i++)
	{
		mean[i] = (psi[i].array().abs2() * xi.array()).sum();
	}
	return mean;
}

VectorXd calc_variance(const vector<VectorXcd> &psi, const VectorXd &mean, const VectorXd &xi)
{
	VectorXd variance(psi.size());
	for (VectorXd::Index i = 0; i < variance.size(); i++)
	{
		variance[i] = (psi[i].array().abs2() * xi.array().abs2()).sum();
	}
	return variance - mean.cwiseAbs2();
}

vector<VectorXcd> calc_p(const vector<VectorXcd> &psi, const MatrixXcd &pOp)
{
	vector<VectorXcd> p(psi.size(), VectorXcd());
	for (size_t i = 0; i < p.size(); i++)
	{
		p[i] = pOp * psi[i];
	}
	return p;
}

VectorXd calc_p_mean(const vector<VectorXcd> &p, const vector<VectorXcd> &psi)
{
	VectorXcd mean(p.size());
	for (VectorXcd::Index i = 0; i < mean.size(); i++)
	{
		mean[i] = psi[i].dot(p[i]);
	}
	return mean.real();
}

VectorXd calc_p_variance(const vector<VectorXcd> &p, const vector<VectorXcd> &psi, const VectorXd &mean, const MatrixXcd &pOp)
{
	VectorXcd variance(p.size());
	for (VectorXcd::Index i = 0; i < variance.size(); i++)
	{
		variance[i] = psi[i].dot(pOp * p[i]);
	}
	return variance.real();// - mean.cwiseAbs2();
}

int main()
{
	double dt = 0.05;
	double dx = 0.1;
	size_t T = 20 * 1000;
	size_t N = 10 * 20 + 1;

	double xi0 = 1;
	double sigma = 1;
	VectorXd xi = VectorXd::LinSpaced(Eigen::Sequential, N, -10, 10);

	MatrixXcd tmpU = MatrixXcd::Zero(N, N);
	tmpU.topRightCorner(N - 1, N - 1) = MatrixXcd::Identity(N - 1, N - 1);
	MatrixXcd tmpL = MatrixXcd::Zero(N, N);
	tmpL.bottomLeftCorner(N - 1, N - 1) = MatrixXcd::Identity(N - 1, N - 1);
	MatrixXcd tmpR = xi.unaryExpr([](double xi) {return xi *xi;}).cast<C>().asDiagonal();
	MatrixXcd H = - 1. / (dx * dx) * (tmpU + tmpL - 2 * MatrixXcd::Identity(N, N)) + dx * dx * tmpR;

	VectorXcd psi0 = xi.unaryExpr([xi0, sigma](double xi) {return exp(- 1. / (2. * sigma) * (xi - xi0) * (xi - xi0));}).cast<C>().normalized();

	vector<VectorXcd> psi = crank_nicholson(H, psi0, dt, T);

	VectorXd norm = calc_norm(psi);
	VectorXd mean = calc_mean(psi, xi);
	VectorXd variance = calc_variance(psi, mean, xi);

	MatrixXcd pOp = MatrixXcd::Zero(N, N);
	pOp.topRightCorner(N - 1, N - 1) += MatrixXcd::Identity(N - 1, N - 1);
	pOp -= MatrixXcd::Identity(N, N);
	pOp /= 2. * C(0, 1) * dx;

	vector<VectorXcd> p = calc_p(psi, pOp);
	VectorXd p_mean = calc_p_mean(p, psi);
	VectorXd p_variance = calc_p_variance(p, psi, p_mean, pOp);

	ofstream out("psi.csv");
	for (size_t t = 0; t < T; t++)
	{
		out << psi[t].cwiseAbs2().transpose() << endl;
	}
	out.close();

	out.open("meas.csv");
	out << norm.transpose() << endl;
	out << mean.transpose() << endl;
	out << variance.transpose() << endl;
	out << p_mean.transpose() << endl;
	out << p_variance.transpose() << endl;
	out.close();

	return 0;
}
