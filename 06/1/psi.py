import sys
import numpy as np
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

mpl.rc('text', usetex=False)
mpl.rc('savefig', bbox='standard')
mpl.rc('figure', autolayout=False, figsize=(6, 6))

n = int(sys.argv[1])

psi = np.loadtxt('psi.csv')
xi = np.linspace(-10, 10, len(psi[0]))
t = 0.05 * np.arange(len(psi))

for i in np.split(t[::20], 4)[n]:
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title('t = {: 3.0f}'.format(i))
    ax.plot(xi, psi[i], 'r-')
    ax.set_xlim(-10, 10)
    ax.set_ylim(0, 0.1)
    fig.savefig('{:03.0f}.png'.format(i))
