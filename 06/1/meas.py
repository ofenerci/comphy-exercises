import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

norm, mean, variance, p_mean, p_variance = np.loadtxt("meas.csv")
t = 0.05 * np.arange(len(norm))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, norm, 'b-', label=r"$|\psi|^2$")
ax.plot(t, mean, 'r-', label=r"$\langle \xi \rangle$")
ax.plot(t, variance, 'g-', label=r"$\langle \xi^2 \rangle - \langle \xi \rangle^2$")
ax.set_xlabel(r"$\tau$")
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3, mode="expand", borderaxespad=0.)
fig.savefig("meas.pdf")

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, p_mean, 'b-', label=r"$\langle p \rangle$")
ax.plot(t, p_variance, 'r-', label=r"$\langle p^2 \rangle - \langle p \rangle^2$")
ax.plot(t, np.sqrt(variance * p_variance), 'g-', label=r"$\Delta x \, \Delta p$")
ax.set_xlabel(r"$\tau$")
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3, mode="expand", borderaxespad=0.)
fig.savefig("meas2.pdf")
