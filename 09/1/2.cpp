#include <cmath>
#include <cstdio>
#include <functional>
#include <iostream>
#include <random>
#include <vector>

#include <Eigen/Dense>

using namespace std;

using namespace Eigen;

double a(size_t N)
{
	size_t N_circle = 0;
	uniform_real_distribution<double> dist(0, 1.0);
	mt19937 gen;
	function<double()> random = bind(dist, gen);
	for (size_t n = 0; n < N; n++)
	{
		double x = random();
		double y = random();
		double norm = pow(x, 2) + pow(y, 2);
		if (norm < 1)
		{
			N_circle++;
		}
	}
	return 4 * (double) N_circle / N;
}

void b()
{
	FILE *file = fopen("2b.csv", "w");
	for (size_t k = 1; k <= 6; k++)
	{
		size_t N = pow(10, k);
		fprintf(file, "%.20f\n", abs(a(N) - M_PI));
	}
	fclose(file);
}

double c_d(size_t N, function<double (double, double)> f,double a, double b)
{
	double N_circle = 0;
	uniform_real_distribution<double> dist(-1, 1.0);
	mt19937 gen;
	function<double()> random = bind(dist, gen);
	for (size_t n = 0; n < N; n++)
	{
		double x = random();
		double y = random();
		double norm = pow(x, 2) + pow(y, 2);
		if (norm < 1)
		{
			N_circle += f(x * a, y * b) * 4 * a * b / N;
		}
	}
	return N_circle;
}

template<typename T>
T MC_integration(function<T(const VectorXd &)> f, const VectorXd &a, const VectorXd &b, size_t N, T zero)
{
	T result = zero;
	uniform_real_distribution<double> dist(0, 1.0);
	mt19937 gen;
	function<double()> random = bind(dist, gen);
	for (size_t n = 0; n < N; n++)
	{
		VectorXd x = a + VectorXd::Zero(a.size()).unaryExpr([&random](double x) { return random(); }).cwiseProduct(b - a);
		result += f(x) / N * (b - a).prod();
	}
	return result;
}

int main()
{
	size_t N = 10000000;
	cout << a(N) << endl;
	b();
	function<double (double, double)> f = [](double x, double y) {return exp(-x * x);};
	cout << c_d(N, f, sqrt(2), 1) << endl;
	double x0 = sqrt(2);
	double y0 = 1;
	VectorXd a(2);
	VectorXd b(2);
	a << -x0, -y0;
	b << x0, y0;
	cout << MC_integration<double>([x0, y0](const VectorXd &x) -> double
	{
		if (pow(x[0] / x0, 2) + pow(x[1] / y0, 2) >= 1)
		{
			return 0;
		}
		return exp(- pow(x[0], 2));
	}, a, b, N, 0) << endl;
	return 0;
}
