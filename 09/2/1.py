import matplotlib.pyplot as plt
import numpy as np

mean_x, mean_y, r_2 = np.loadtxt("1.txt", unpack=True)
m10 = np.loadtxt("1_c_10.txt")
m100 = np.loadtxt("1_c_100.txt")
m1000 = np.loadtxt("1_c_1000.txt")

t = np.linspace(0, 1000, len(mean_x))
plt.plot(t, mean_x, 'r.', label=r"$\langle x \rangle$")
plt.plot(t, mean_y, 'b.', label=r"$\langle y \rangle$")
plt.legend(loc="best")
plt.xlabel("$t$")
plt.savefig("1xy.pdf")
plt.clf()

plt.plot(t, r_2)
plt.legend(loc='best')
plt.xlabel("$t$")
plt.ylabel(r"$\langle r^2 \rangle$")
plt.savefig("1rr.pdf")
plt.clf()

plt.imshow(m10[900:1100, 900:1100], extent=(900, 1100, 900, 1100))
plt.colorbar()
plt.savefig("m10.pdf")
plt.clf()

plt.imshow(m100[900:1100, 900:1100], extent=(900, 1100, 900, 1100))
plt.colorbar()
plt.savefig("m100.pdf")
plt.clf()

plt.imshow(m1000[900:1100, 900:1100], extent=(900, 1100, 900, 1100))
plt.colorbar()
plt.savefig("m1000.pdf")
plt.clf()
