#include <cmath>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

void work()
{
    mt19937_64 rand(42);
    uniform_int_distribution<int> uniform(0, 3);

    vector<vector<int>> xs(1001, vector<int>(1000, 0));
    vector<vector<int>> ys(1001, vector<int>(1000, 0));
    vector<double> mean_x(1000, 0);
    vector<double> mean_y(1000, 0);
    vector<double> mean_x2(1000, 0);
    vector<double> mean_y2(1000, 0);

    FILE* outfile = fopen("1.txt", "w");

    for (size_t t = 1; t <= 1000; t++)
    {
        for(size_t n = 0; n < 1000; n++)
        {
            int d = uniform(rand);
            xs[t][n] = xs[t-1][n];
            ys[t][n] = ys[t-1][n];
            switch(d)
            {
                case 0:
                    xs[t][n] += 1;
                    break;
                case 1:
                    xs[t][n] -= 1;
                    break;
                case 2:
                    ys[t][n] += 1;
                    break;
                case 3:
                    ys[t][n] -= 1;
                    break;
            }
            mean_x[t] += xs[t][n];
            mean_y[t] += ys[t][n];
            mean_x2[t] += xs[t][n] * xs[t][n];
            mean_y2[t] += ys[t][n] * ys[t][n];
        }
        mean_x[t] /= 1000;
        mean_y[t] /= 1000;
        mean_x2[t] /= 1000;
        mean_y2[t] /= 1000;
        fprintf(outfile, "%.15f %.15f %.15f\n", mean_x[t], mean_y[t], mean_x2[t] + mean_y2[t]);
    }
    fclose(outfile);

    vector<vector<int>> histo10(2001, vector<int>(2001, 0));
    vector<vector<int>> histo100(2001, vector<int>(2001, 0));
    vector<vector<int>> histo1000(2001, vector<int>(2001, 0));

    FILE* outfile10 = fopen("1_c_10.txt", "w");
    FILE* outfile100 = fopen("1_c_100.txt", "w");
    FILE* outfile1000 = fopen("1_c_1000.txt", "w");

    for (size_t i = 0; i < 1000; i++)
    {
		histo10[xs[10][i] + 1000][ys[10][i] + 1000]++;
		histo100[xs[100][i] + 1000][ys[100][i] + 1000]++;
		histo1000[xs[1000][i] + 1000][ys[1000][i] + 1000]++;
	}

	for (size_t y = 0; y <= 2000; y++)
	{
		for (size_t x = 0; x <= 2000; x++)
		{
			fprintf(outfile10, "%d ", histo10[y][x]);
			fprintf(outfile100, "%d ", histo100[y][x]);
			fprintf(outfile1000, "%d ", histo1000[y][x]);
		}
		fprintf(outfile10, "\n");
		fprintf(outfile100, "\n");
		fprintf(outfile1000, "\n");
	}
}

int main()
{
    work();
    return 0;
}
