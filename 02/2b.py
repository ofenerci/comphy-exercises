import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

q, I = np.loadtxt("2b.csv", unpack=True)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(q, I, 'b-')
ax.set_xlabel("$q$")
ax.set_ylabel("$I(q)$")
fig.savefig('2b.pdf')
