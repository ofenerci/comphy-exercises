#include <functional>

#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

void runge_kutta(std::function<void(double x, double *y, double *yd)> f, int D, int N, double x_0, double x_n, double **y);

#endif
