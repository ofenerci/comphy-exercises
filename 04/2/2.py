import matplotlib.pyplot as plt
import numpy as np

t, E_kin, E_pot, E_ges = np.loadtxt("2a.csv", usecols=(0,3,4,5), unpack=True)

plt.subplot(2,1,1)
plt.plot(t, E_kin, label="Kinetische Energie")
plt.plot(t, E_pot, label="potentielle Energie")
plt.plot(t, E_ges, label="Gesamtenergie")
plt.legend(loc="best")
plt.xlabel("$t$")
plt.ylabel(r"$E$")

plt.subplot(2,1,2)
plt.xlabel("$t$")
plt.ylabel(r"$E$")
plt.plot(t, E_ges, label="Gesamtenergie")
plt.legend()
plt.savefig("2a.pdf")
plt.clf()

plt.subplot(2,1,1)
for u in [0.5, 1.0, 1.2]:
    t, x, v = np.loadtxt("2b-{:.1f}.csv".format(u), unpack=True)
    plt.plot(x / np.pi, v, label="$Q = {:.1f}$".format(u))

plt.ylabel(r"$\dot{\theta}$")
plt.xlabel(r"$\theta$")
plt.legend(loc='best')

plt.subplot(2,1,2)
for u in [1.3, 1.4]:
    t, x, v = np.loadtxt("2b-{:.1f}.csv".format(u), unpack=True)
    plt.plot(x / np.pi, v, label="$Q = {:.1f}$".format(u))

plt.ylabel(r"$\dot{\theta}$")
plt.xlabel(r"$\theta$")
plt.legend(loc='best')
plt.savefig("2b.pdf")
