#include <cmath>
#include <cstdio>
#include <functional>

#include "rungekutta.h"

using namespace std;

void a()
{
    auto f = [](double x, double *y, double *yd)
    {
        yd[0] = y[1];
        yd[1] = -y[0];
    };

    unsigned int N = 400;
    double x_0 = 0;
    double x_n = 2 * M_PI;
    double h = (x_n - x_0) / N;
    double **ys = new double *[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }

    ys[0][0] = 1;
    ys[0][1] = 0;

    runge_kutta(f, 2, N, x_0, x_n, ys); 

    FILE *outfile = fopen("1a.csv", "w");
    for (size_t i = 0; i <= N; i++)
    {
        fprintf(outfile, "%.10f %.10f %.10f %.10f\n", x_0 + h * i, ys[i][0], ys[i][1], 0.5 * (ys[i][0] * ys[i][0] + ys[i][1] * ys[i][1]));
    }
    fclose(outfile);
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

void b(double A, double w, double Q)
{
    auto f = [A, w, Q](double x, double *y, double *yd)
    {
        yd[0] = y[1];
        yd[1] = - y[1] / Q - y[0] + A * cos(w * x);
    };

    unsigned int N = 10000;
    double x_0 = 0;
    double x_n = 50;
    double h = (x_n - x_0) / N;
    double **ys = new double *[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }

    ys[0][0] = 0;
    ys[0][1] = 0;

    runge_kutta(f, 2, N, x_0, x_n, ys); 

    char *name = new char[12];
    sprintf(name, "1b-%.2f.csv", Q);
    FILE *outfile = fopen(name, "w");
    delete[] name;
    for (size_t i = 0; i <= N; i++)
    {
        fprintf(outfile, "%.10f %.10f %.10f\n", x_0 + h * i, ys[i][0], ys[i][1]);
    }
    fclose(outfile);
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

int main()
{
    a();
    b(1.5, 0.666666666, 0.25);
    b(1.5, 0.666666666, 0.50);
    b(1.5, 0.666666666, 1.00);
}
