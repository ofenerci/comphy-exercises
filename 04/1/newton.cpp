#include <functional>

using namespace std;

void rungekutta4(function<void(double, double*, double*)> f, int D, int N, double x0, double x1, double **y)
{
	double h = (x1 - x0) / N;

	double *k1 = new double[D];
	double *k2 = new double[D];
	double *k3 = new double[D];
	double *k4 = new double[D];
	double *tmp = new double[D];

	for (int i = 1; i <= N; i++)
	{
		double x = x0 + i * h;

		f(x, y[i - 1], k1);
		for (int j = 0; j < D; j++)
		{
			k1[j] *= h;
			tmp[j] = y[i - 1][j] + 0.5 * k1[j];
		}

		f(x + 0.5 * h, tmp, k2);
		for (int j = 0; j < D; j++)
		{
			k2[j] *= h;
			tmp[j] = y[i - 1][j] + 0.5 * k2[j];
		}

		f(x + 0.5 * h, tmp, k3);
		for (int j = 0; j < D; j++)
		{
			k3[j] *= h;
			tmp[j] = y[i - 1][j] + k3[j];
		}

		f(x + h, tmp, k4);
		for (int j = 0; j < D; j++)
		{
			k4[j] *= h;
			y[i][j] = y[i - 1][j] + 1. / 6 * (k1[j] + 2 * (k2[j] + k3[j]) + k4[j]);
		}
	}

	delete[] k1;
	delete[] k2;
	delete[] k3;
	delete[] k4;
	delete[] tmp;
}

void newton(function<void(double, double*, double*)> F, int N, int D, double t1, double **rv)
{
	rungekutta4([F, D](double t, double *rv, double *f)
	{
		for (int i = 0; i < D; i++)
		{
			f[i] = rv[i + D];
		}
		F(t, rv, f + D);
	}, 2 * D, N, 0, t1, rv);
}
